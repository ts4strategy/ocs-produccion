({
    doInit : function(cmp, evt, hlp){
        console.log("Aura Component: PostalCodeModal - Controller Method: doInit");
        /*let getPicklistState = cmp.get("c.getSobjects");
        let query = "SELECT d_estado__c FROM TS4_CatalogoCP__c GROUP BY d_estado__c";
        getPicklistState.setParams({"query" : query});
        hlp.promesa(cmp, getPicklistState)
        .then($A.getCallback(function(resStates){
            console.log("1. The States of Mexico has been retrieved: ");
            console.log(JSON.parse(JSON.stringify(resStates)));
            
            let states = [];
            resStates.forEach( function(state) { states.push(state.d_estado__c); });
            cmp.set("v.states", states);
        }))*/
    },
    
    selectState : function(cmp, evt, hlp){
        console.log("Aura Component: PostalCodeModal - Controller Method: selectState");
        let getPicklistCity = cmp.get("c.getSobjects");
        let query = "SELECT d_ciudad__c FROM TS4_CatalogoCP__c WHERE d_estado__c = '"+cmp.get("v.postalCode").d_estado__c+"' GROUP BY d_ciudad__c";
        getPicklistCity.setParams({"query" : query});
        console.log(query);
        hlp.promesa(cmp, getPicklistCity)
        .then($A.getCallback(function(resCities){
            console.log("2. The Cities has been retrieved: ");
            console.log(JSON.parse(JSON.stringify(resCities)));
            
            let cities = [];
            resCities.forEach( function(city) { cities.push(city.d_ciudad__c); });
            cmp.set("v.cities", cities);
        }))
    },
    
    selectCity : function(cmp, evt, hlp){ //alcsMpios
        console.log("Aura Component: PostalCodeModal - Controller Method: selectState");
        let getPicklistAlcMpio = cmp.get("c.getSobjects");
        let query = "SELECT D_mnpio__c FROM TS4_CatalogoCP__c WHERE d_estado__c = '"+cmp.get("v.postalCode").d_estado__c+"' AND d_ciudad__c = '"+cmp.get("v.postalCode").d_ciudad__c+"' GROUP BY D_mnpio__c";
        getPicklistAlcMpio.setParams({"query" : query});
        hlp.promesa(cmp, getPicklistAlcMpio)
        .then($A.getCallback(function(resAlcsMpios){
            console.log("3. The Municipios has been retrieved: ");
            console.log(JSON.parse(JSON.stringify(resAlcsMpios)));
            
            let alcsMpios = [];
            resAlcsMpios.forEach( function(alcMpio) { alcsMpios.push(alcMpio.D_mnpio__c); });
            cmp.set("v.alcsMpios", alcsMpios);
        }))
    },
    
    submitDetails: function(component, event, helper) {
      // Set isModalOpen attribute to false
      //Add your code to call apex method or do some processing
       var con = component.get("v.postalCode");
       con.Name = con.d_codigo__c + ' - ' + con.d_asenta__c;
       
       const createPostalCode = component.get("c.createPostalCode");
       createPostalCode.setParams({"cp" : con});
       
       helper.promesa(component, createPostalCode)
       .then($A.getCallback(function(response){
           console.log("Aura Component: PostalCodeModal - Controller Method: submitDetails");
           //Para el Lookup
           let cmpEvent = component.getEvent("newRecordInsertEvent"); 
           cmpEvent.setParams({ "nameObject" : "TS4_CatalogoCP__c", "newRecord" : response}); 
           cmpEvent.fire();

           var toastEvent = $A.get("e.force:showToast");
           toastEvent.setParams({
               "title": "Se ha creado el código postal",
               "message": "Se ha creado el código postal"
           });
           toastEvent.fire();
       }))
       
       
       //component.set("v.isModalOpen", false);
       //component.set("v.postalCode", {});
   },
})