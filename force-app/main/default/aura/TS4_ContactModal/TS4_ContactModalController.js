({  
   closeModel: function(component, event, helper) {
      // Set isModalOpen attribute to false  
      component.set("v.isModalOpen", false);
       component.set("v.contact", null);
   },
  
   submitDetails: function(component, event, helper) {
      // Set isModalOpen attribute to false
      //Add your code to call apex method or do some processing
       var con = component.get("v.contact");
       con.AccountId = component.get("v.account").Id;
       
       
       const createContact = component.get("c.createContact");
       createContact.setParams({"con" : con});
       
       helper.promesa(component, createContact)
       .then($A.getCallback(function(response){
          
           console.log("Contacto creado");
           //Para el Lookup
           let cmpEvent = component.getEvent("newRecordInsertEvent"); 
           cmpEvent.setParams({ "nameObject" : component.get('v.contactType'), "newRecord" : response}); 
           cmpEvent.fire();
           
           var toastEvent = $A.get("e.force:showToast");
           toastEvent.setParams({
               "title": "Se ha creado el Contacto",
               "message": "Ahora puedes encontrarlo en la caja de Contactos"
           });
           toastEvent.fire();
       }))
       
       component.set("v.isModalOpen", false);
       component.set("v.contact", {});
   },
})