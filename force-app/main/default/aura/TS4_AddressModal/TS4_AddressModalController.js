({  
   closeModel: function(component, event, helper) {
      component.set("v.isModalOpen", false);
       component.set("v.address", null);
       component.set("v.lookupPostalCode", null);
   },
  
   submitDetails: function(component, event, helper) {
       var con = component.get("v.address");
       con.Name = component.get("v.lookupPostalCode").d_codigo__c + ' - ' + component.get("v.lookupPostalCode").d_asenta__c;
       con.Cuenta__c = component.get("v.account").Id;
       con.TS4_CodigoPostal__c = component.get("v.lookupPostalCode").Id;
       
       const createAddress = component.get("c.createAddress");
       
       console.log('-------kkkkk-----');
       console.log(JSON.parse(JSON.stringify(con)));

       createAddress.setParams({"address" : con});
       
       helper.promesa(component, createAddress)
       .then($A.getCallback(function(response){
           let cmpEvent = component.getEvent("newRecordInsertEvent"); 
           cmpEvent.setParams({ "nameObject" : "TS4_Direcciones__c", "newRecord" : response}); 
           cmpEvent.fire();

           var toastEvent = $A.get("e.force:showToast");
           toastEvent.setParams({
               "title": "Se ha creado la Dirección",
               "message": "Se ha creado la Dirección"
           });
           toastEvent.fire();
       }))
       
       component.set("v.isModalOpen", false);
       component.set("v.address", {});
       component.set("v.lookupPostalCode", null);
   },
    
    handleComponentEvent : function(cmp, event, helper) {
        // get the selected Account record from the COMPONETN event 	 
        var flagModal = event.getParam("sObjectName");
        
        console.log("En lo cierto");
        if(flagModal === 'TS4_CatalogoCP__c'){
            cmp.set("v.isModalOpen", false);
            cmp.set("v.postalCodeModal", true);
        }
    },
    
    newRecordLookup : function(cmp, event, hlp){
        var nameModalObj = event.getParam("nameObject");
        var newRecord = event.getParam("newRecord");
        if(nameModalObj == 'TS4_CatalogoCP__c'){
            cmp.set("v.isModalOpen", true);
            cmp.find("pcModal").set("v.isModalOpen", false);
            cmp.find("pcModal").set("v.postalCode", {});
            cmp.set("v.lookupPostalCode", newRecord);
            cmp.find("LookupCP").editMode();
            
        }
    },
    
    closeChildModal : function(cmp, evt, hlp){
        cmp.set("v.isModalOpen", true);
        cmp.find("pcModal").set("v.isModalOpen", false);
        cmp.find("pcModal").set("v.postalCode", {});
    }
})