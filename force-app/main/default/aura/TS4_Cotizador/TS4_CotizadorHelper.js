({    
    promesa : function(component, action){
        return new Promise( function(resolve, reject){
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    resolve(response.getReturnValue());
                } else if (state === "ERROR") {
                    //reject(new Error(response.getError()));
                    let errors = response.getError();
                    reject(new Error(errors && Array.isArray(errors) && errors.length === 1 ? errors[0].message : JSON.stringify(errors)));
                }
            });
            $A.enqueueAction(action);
        });
    }, 
    
    cargaDatosMaquina : function(component,mapvalues) {
        let max =  mapvalues.length;
        mapvalues.forEach(function(position){
            let Config = component.get("c.getConfigList");
            Config.setParams({ idmachene: position.lookupMaquina.TS4_Maquina__c});  
            Config.setCallback(this, function(result){
                var configura = result.getReturnValue();
                var lstConfiguraciones = [];
                for(var i=0;i<configura.length; i++){
                    var configuracionAPEX = {};
                    configuracionAPEX.Name = configura[i].TS4_Configuracion__c;
                    if(position.lookupMaquina.configuracion == configura[i].TS4_Configuracion__c){
                        configuracionAPEX.Selected = true;
                    }else
                    {
                        configuracionAPEX.Selected = false;                        
                    }
                    lstConfiguraciones.push(configuracionAPEX);
                }
                position.lstconfig = lstConfiguraciones;
                console.log(position);
                let vaso = component.get("c.getListVasos");
                vaso.setParams({ idmachene: position.lookupMaquina.TS4_Maquina__c});  
                vaso.setCallback(this, function(result){
                    var vasos = result.getReturnValue();
                    var lstvasos = [];
                    if(vasos != null){
                        for(var i=0;i<vasos.length; i++){
                            var vasosAPEX = {};
                            vasosAPEX.Name = vasos[i];
                            console.log(position.lookupMaquina.tipovaso)
                            console.log(vasosAPEX.Name)
                            if(position.lookupMaquina.tipovaso == vasosAPEX.Name){
                                vasosAPEX.Selected = true;
                            }else{
                                vasosAPEX.Selected = false;                        
                            }
                            lstvasos.push(vasosAPEX);
                            console.log(lstvasos)
                        }
                        position.lstVasos = lstvasos;
                        component.set("v.replicregis", mapvalues);
                    }
                });    
                $A.enqueueAction(vaso);
            });
            $A.enqueueAction(Config);
        });
    },
})