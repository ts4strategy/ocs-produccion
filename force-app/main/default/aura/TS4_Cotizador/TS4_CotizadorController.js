({
    setGuardar : function(component,event,helper){
        let vacio = 'false'; 
        let tipoguradar = event.getSource().get("v.name");
        let mapvalues = component.get("v.replicregis");
        if(mapvalues.length == 0){
            vacio = 'MVM';
        }
        let lokproduct = component.get("v.lookupProduct");
        let saveQuote = [];
        if(tipoguradar == 'cnc'){
            let maplstAccesorio = []; 
            let quoteIteration = {};
            for(var a= 0; a<mapvalues.length; a++){
                let position = mapvalues[a];
                let recibidos  = position.lstAccesorios;
                let quoteIteration = {};
                let qlis = {};
                qlis.QuoteId  = component.get('v.recordId');
                qlis.Product2Id = mapvalues[a].Id;
                qlis.PricebookEntryId = mapvalues[a].IdPriceEntry;
                qlis.UnitPrice = mapvalues[a].Unitprice;
                qlis.TS4_Maquina__c = mapvalues[a].lookupMaquina.TS4_Maquina__c;
                qlis.Quantity = 1;
                qlis.sObjectType = 'QuoteLineItem';
                quoteIteration.qli = qlis;
                let recordPRM = {};
                if(mapvalues[a].lookupMaquina.TS4_Maquina__c == undefined){
                    vacio = 'VM';
                }
                recordPRM.TS4_Producto__c  = mapvalues[a].lookupMaquina.TS4_Maquina__c;
                //Se creo tipo de vaso en producto y productos relacionados
                recordPRM.TS4_Tipo_de_vaso__c  = mapvalues[a].lookupMaquina.tipovaso;
                //Se creo sucursal y productos relacionados
                recordPRM.TS4_Sucursal__c = mapvalues[a].lookupMaquina.sucursal;
                //Se creo subsidio y productos relacionados
                recordPRM.TS4_Subsidio__c = mapvalues[a].lookupMaquina.subsidio;
                //Se creoconsumidor y productos relacionados
                recordPRM.TS4_Consumidor__c = mapvalues[a].lookupMaquina.consumidor;
                //Se creo toma de agua en producto y productos relacionados
                recordPRM.TS4_Toma_de_agua__c = mapvalues[a].lookupMaquina.toma ;
                //Se creo configuración del producto y productos relacionados
                recordPRM.TS4_Configuracion__c  = mapvalues[a].lookupMaquina.configuracion;
                recordPRM.TS4_Producto_de_cotizacion__c = '';
                recordPRM.TS4_Tipo_QuoteLine__c  = 'Maquina' ;
                quoteIteration.maquina = recordPRM;
                let maplstProducts = mapvalues[a].lstProduct;
                quoteIteration.productos = [];
                if(maplstProducts.length == 0){
                    vacio = 'VC';
		        }                    
                for(var e= 0; e<maplstProducts.length; e++){    
                    let recordPRP = {};
                    recordPRP.TS4_Precio_Maquina__c = maplstProducts[e].TS4_Costo_maquina__c;
                    recordPRP.TS4_Precio_Cliente__c = maplstProducts[e].Costo__c;
                    recordPRP.TS4_Producto__c= maplstProducts[e].Id;
                    recordPRP.TS4_Producto_de_cotizacion__c = '';
                    recordPRP.TS4_Tipo_QuoteLine__c  = 'Producto' ;
                    quoteIteration.productos.push(recordPRP);
                }  
                let maplstAccesorios = mapvalues[a].lstAccesorios;
                quoteIteration.consumibles = [];
                for(var e = 0; e<maplstAccesorios.length; e++){
                    let recordPRP = {};
                    recordPRP.TS4_Cortesia__c  = maplstAccesorios[e].Cortesia__c ;
                    recordPRP.TS4_Precio_Cliente__c = maplstAccesorios[e].Costo__c ;
                    recordPRP.TS4_Producto__c = maplstAccesorios[e].TS4_Producto__c;
                    recordPRP.TS4_Tipo_QuoteLine__c  = 'Consumible' 
                    recordPRP.TS4_Cantidad__c  =  maplstAccesorios[e].Cantidad__c;
                    recordPRP.TS4_Producto_de_cotizacion__c = '';
                    quoteIteration.consumibles.push(recordPRP);
                }
                saveQuote.push(quoteIteration);     
            }
        }
        else{
            let maplstAccesorio = []; 
            let quoteIteration = {};
            for(var a= 0; a<mapvalues.length; a++){
                let position = mapvalues[a];
                let recibidos  = position.lstAccesorios;
                let quoteIteration = {};
                let qlis = {};
                qlis.QuoteId  = component.get('v.recordId');
                qlis.Product2Id = mapvalues[a].Id;
                qlis.PricebookEntryId = mapvalues[a].IdPriceEntry;
                qlis.UnitPrice = mapvalues[a].Unitprice;
                qlis.TS4_Maquina__c = mapvalues[a].lookupMaquina.TS4_Maquina__c;
                qlis.Quantity = 1;
                qlis.sObjectType = 'QuoteLineItem';
                quoteIteration.qli = qlis;
                let recordPRM = {};
                if(mapvalues[a].lookupMaquina.TS4_Maquina__c == undefined){
                    vacio = 'VM';
                }
                recordPRM.TS4_Producto__c  = mapvalues[a].lookupMaquina.TS4_Maquina__c;
                //Se creo tipo de vaso en producto y productos relacionados
                recordPRM.TS4_Tipo_de_vaso__c  = mapvalues[a].lookupMaquina.tipovaso;
                //Se creo sucursal y productos relacionados
                recordPRM.TS4_Sucursal__c = mapvalues[a].lookupMaquina.sucursal;
                //Se creo subsidio y productos relacionados
                recordPRM.TS4_Subsidio__c = mapvalues[a].lookupMaquina.subsidio;
                //Se creoconsumidor y productos relacionados
                recordPRM.TS4_Consumidor__c = mapvalues[a].lookupMaquina.consumidor;
                //Se creo toma de agua en producto y productos relacionados
                recordPRM.TS4_Toma_de_agua__c = mapvalues[a].lookupMaquina.toma;
                recordPRM.TS4_Producto_de_cotizacion__c = '';
                recordPRM.TS4_Tipo_QuoteLine__c  = 'Maquina' ;
                quoteIteration.maquina = recordPRM;
                let maplstProducts = mapvalues[a].lstProduct;
                quoteIteration.productos = [];
                for(var e= 0; e<maplstProducts.length; e++){    
                    let recordPRP = {};
                    recordPRP.TS4_Precio_Maquina__c = maplstProducts[e].TS4_Costo_maquina__c;
                    recordPRP.TS4_Precio_Cliente__c = maplstProducts[e].Costo__c;
                    recordPRP.TS4_Producto__c= maplstProducts[e].Id;
                    recordPRP.TS4_Producto_de_cotizacion__c = '';
                    recordPRP.TS4_Tipo_QuoteLine__c  = 'Producto' ;
                    quoteIteration.productos.push(recordPRP);
                }  
                let maplstAccesorios = mapvalues[a].lstAccesorios;
                quoteIteration.consumibles = [];
                for(var e = 0; e<maplstAccesorios.length; e++){
                    let recordPRP = {};
                    recordPRP.TS4_Cortesia__c  = maplstAccesorios[e].Cortesia__c ;
                    recordPRP.TS4_Precio_Cliente__c = maplstAccesorios[e].Costo__c ;
                    recordPRP.TS4_Producto__c = maplstAccesorios[e].TS4_Producto__c;
                    recordPRP.TS4_Tipo_QuoteLine__c  = 'Consumible' ;
                    recordPRP.TS4_Producto_de_cotizacion__c = '';
                    recordPRP.TS4_Cantidad__c  =  maplstAccesorios[e].Cantidad__c;
                    quoteIteration.consumibles.push(recordPRP);
                }
                saveQuote.push(quoteIteration);     
            }
        }
        if(vacio == 'false'){
            var action = component.get("c.saveDataQuote");
            action.setParams({ dataCotizador: saveQuote });
            action.setCallback(this, function(response){
                var state = response.getState();
                console.log(state)
                var guardado = response.getReturnValue();
                console.log(guardado)
                if(guardado.status == "OK"){
                    component.find('notifLib').showToast({ "variant" : "success", "title": "Guardado exitoso", "message": response.message });            
                }
                else{
                    component.find('notifLib').showToast({ "variant" : "error", "title": "Error al guardar", "message": response.message });
                }
            });
            $A.enqueueAction(action);
        }
        else{
            if(vacio == 'VC'){
 	           component.find('notifLib').showToast({ "variant" : "warning", "title": "No se ha seleccionado alguna configuración"});
            }
            else{
                if(vacio == 'MVM'){
	               component.find('notifLib').showToast({ "variant" : "warning", "title": "No se ha seleccionado algun servicio, no se guardado ningun cambio"});                
                }
                else{
	               component.find('notifLib').showToast({ "variant" : "warning", "title": "No se ha seleccionado alguna maquina"});                
                }
            }
        }
    },
    
    deleteItem: function(component,event,helper){
        let eliminar = event.getSource().get("v.name");
        let indxdt = eliminar.replace('dt', '');
        var items = component.get('v.replicregis');
        items.splice(indxdt, 1);
        component.set('v.replicregis', items);      
    },
    
    deleteItemAccess: function(component,event,helper){
        let eliminar = event.getSource().get("v.name");
        let indxdt = eliminar.replace('dts', '');
        var mapvalues = component.get('v.replicregis');
        let position = mapvalues[indxdt];
        let recibidos  = position.lstAccesorios;
        let eliminardti = event.getSource().get("v.title");
        let indexdt = eliminardti.replace('dti', '');
        position.lstAccesorios.splice(indexdt, 1);
        mapvalues[indxdt] = position;
        component.set("v.replicregis", mapvalues);
    },
    
    setCotizar : function(component, event, helper) {
        let indxbuttonidx = event.getSource().get("v.name");
        let selectconfig = event.getSource().get("v.value");
        let indxbutton = indxbuttonidx.replace('opt', '');
        let mapvalues = component.get("v.replicregis");
        let position = mapvalues[indxbutton];
        var action = component.get("c.getProduct");
        action.setParams({ configlst: selectconfig });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var result = response.getReturnValue();
                var lstConfiguraciones = [];
                for(var e = 0 ;  e< position.lstconfig.length; e++){
                    var configuracionAPEX = {};
                    configuracionAPEX.Name = position.lstconfig[e].Name;
                    if(configuracionAPEX.Name == selectconfig){
                        configuracionAPEX.Selected = true;
                    }     
                    else{
                        configuracionAPEX.Selected = false;                        
                    }
                    lstConfiguraciones.push(configuracionAPEX);
                }
                var lstvasos = [];
                if(position.lstVasos.length !=0){
                    for(var e = 0 ;  e< position.lstVasos.length; e++){
                        var vasosAPEX = {};
                        vasosAPEX.Name = position.lstVasos[e].Name;
                        if(vasosAPEX.Name == position.lookupMaquina.tipovaso){
                            vasosAPEX.Selected = true;
                        }     
                        else{
                            vasosAPEX.Selected = false;                        
                        }
                        lstvasos.push(vasosAPEX);
                    }
                position.lstVasos = lstvasos;
                }
                position.lstconfig = lstConfiguraciones;
                position.lstProduct = result;
                mapvalues[indxbutton] = position;
                component.set("v.replicregis", mapvalues);
            }
        });
        $A.enqueueAction(action);
    },
    
    handleComponentEventMaquina : function(component, event, helper) {
        var selectedMachine = event.getParam("recordByEvent");
        component.set('v.mapMachineId',selectedMachine.index);
        let mapvalues = component.get("v.replicregis");
        let position = mapvalues[selectedMachine.index];
        let ids = selectedMachine.TS4_Maquina__r.Id;
        position.lstProduct = [];
        var action = component.get("c.getConfigList");
        action.setParams({ idmachene: ids });  
        action.setCallback(this, function(result){
            var configura = result.getReturnValue();
            var lstConfiguraciones = [];
            console.log(configura);
            if(configura != null){
                for(var i=0;i<configura.length; i++){
                    var configuracionAPEX = {};
                    configuracionAPEX.Name = configura[i].TS4_Configuracion__c;
                    lstConfiguraciones.push(configuracionAPEX);
                }
                position.lstconfig = lstConfiguraciones;
                mapvalues[selectedMachine.index] = position;
                component.set("v.replicregis", mapvalues);
            }
            else{
                position.lstconfig = [];
                mapvalues[selectedMachine.index] = position;
                component.set("v.replicregis", mapvalues); 
            }
        });
        $A.enqueueAction(action);
        
        var action = component.get("c.getListVasos");
        action.setParams({ idmachene: ids });  
        action.setCallback(this, function(result){
            var vaso = result.getReturnValue();
            if(vaso != null){
                var lstvaso = [];
                for(var i=0;i<vaso.length; i++){
                    var vasoAPEX = {};
                    vasoAPEX.Name = vaso[i];
                    vasoAPEX.selSelected = false;
                    lstvaso.push(vasoAPEX);
                }
                position.lstVasos = lstvaso;
                mapvalues[selectedMachine.index] = position;
                component.set("v.replicregis", mapvalues);
            }
            else{
                position.lstVasos = [];
                mapvalues[selectedMachine.index] = position;
                component.set("v.replicregis", mapvalues);  
            }
        });
        $A.enqueueAction(action);
    },
    
    agregaracesorio : function(component, event, helper){
        let indxbuttonidx = event.getSource().get("v.name");
        let indxbutton = indxbuttonidx.replace('aacs', '');
        let mapvalues = component.get("v.replicregis");
        let position = mapvalues[indxbutton];
        let selectid = position.lookupAccesorio.Id;
        let cantidadaces = position.cantidadaces;
        if(selectid!= undefined ){
            var action = component.get("c.getAccesorio");
            action.setParams({ configlst: selectid });  
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS"){
                    for(var a = 0; a< 1;a++){
                        var result = response.getReturnValue();
                        var objAccesorio = {};
                        objAccesorio.Name = result.Name;
                        objAccesorio.Costo__c = result.Costo__c;
                        objAccesorio.Cantidad__c = cantidadaces;
                        objAccesorio.TS4_Producto__c  = result.Id ;
                        objAccesorio.Cortesia__c = result.Cortesia__c;
                        objAccesorio.ProductRelatId = '';
                        position.lstAccesorios.push(objAccesorio);  
                    }
                }
                let inputaccesorios = component.find("Accesorio");
                if(inputaccesorios.length){
                    inputaccesorios[indxbutton].clear();
                }else{
                    inputaccesorios.clear();
                }
                mapvalues[indxbutton] = position;
                component.set("v.replicregis", mapvalues);
            });
            $A.enqueueAction(action);            
        }
        else{
            component.find('notifLib').showToast({ "variant" : "warning", "title": "No haz seleccionado un consumible"});            
        }
    },
    
    agregar : function(component, event, helper) {    
        var action = component.get("v.lookupProduct");
        let cantidad = component.find('cantidad').get("v.value");
        var mapValuesObj = component.get('v.replicregis');
        if(component.get("v.lookupProduct").Id != undefined ){
            for( var i = 0; i< cantidad; i++){
                mapValuesObj.push({"Id" : action.Product2.Id,"name": action.Product2.Name, "IdPriceEntry": action.Id , "Unitprice": action.UnitPrice, "QuoteLineItemId": '', "lstProduct": [], "lstAccesorios":[], "lookupMaquina": {},"lookupAccesorio": {}, "cantidadaces": 1});
            };
            let boton = 1
            component.set('v.replicregis',mapValuesObj);
            component.set('v.guardar',boton);   
            component.find('notifLib').showToast({ "variant" : "info", "title": "Se ha agregado un nuevo servicio"});            
        }
        else{
            component.find('notifLib').showToast({ "variant" : "warning", "title": "No haz seleccionado un servicio"});
        }
        let inputproduct = component.find("Lookup");
        inputproduct.clear();
        
    },
    
    updpricebook : function(component, event, helper){
        let pricebook = component.get("v.lookupPricebook");
        let registro = component.get("v.registro");
        var updateFlag = false;
        const save_act = component.get("c.saveopp");
        if(component.get("v.lookupPricebook").Id != undefined ){
            save_act.setParams({"pricebookid" : pricebook.Id, "oppid" : registro.OpportunityId, "quoteid": component.get('v.recordId')});
            helper.promesa(component, save_act)
            .then($A.getCallback(function(response){
                if(response.status == 'OK'){
                    component.find('notifLib').showToast({ "variant" : "success", "title": "Guardado exitoso", "message": response.message });            
                }
                else{
                    component.find('notifLib').showToast({ "variant" : "error", "title": "Error al guardar", "message": response.message });
                }
            }))
        }
        else{
            component.find('notifLib').showToast({ "variant" : "warning", "title": "No haz seleccionado una lista de precio"});
        }
        $A.get('e.force:refreshView').fire();
    },
    
    doInit : function(component, event, helper) {
        const getInitData = component.get("c.getInitData");
        getInitData.setParams({"quoteId": component.get('v.recordId')});
        helper.promesa(component, getInitData).then($A.getCallback(function(rltData){
            component.set('v.registro', rltData);
        }));
        
        var mapValuesObj = component.get('v.replicregis');
		var action = component.get("c.getInitDataService");
        action.setParams({ quoteId: component.get('v.recordId') });  
        action.setCallback(this, function(result){
            let resQlin= result.getReturnValue();
            if(resQlin.qli.length != 0){
                for( var a = 0; a< resQlin.qli.length; a++){  
                    var mapValuesObj = component.get('v.replicregis');
                    var verMaquina = {};
                    verMaquina.TS4_Maquina__r ={};
                    verMaquina.TS4_Maquina__c = resQlin.maquina[a].TS4_Producto__r.Id;
                    verMaquina.TS4_Maquina__r.Id = resQlin.maquina[a].TS4_Producto__r.Id;
                    verMaquina.TS4_Maquina__r.Name = resQlin.maquina[a].TS4_Producto__r.Name;
                    verMaquina.toma = resQlin.maquina[a].TS4_Toma_de_agua__c;
                    verMaquina.sucursal = resQlin.maquina[a].TS4_Sucursal__c;
                    verMaquina.subsidio = resQlin.maquina[a].TS4_Subsidio__c;
                    verMaquina.consumidor = resQlin.maquina[a].TS4_Consumidor__c;
                    verMaquina.tipovaso = resQlin.maquina[a].TS4_Tipo_de_vaso__c;
                    verMaquina.configuracion = resQlin.maquina[a].TS4_Configuracion__c;
                    verMaquina.modoEdicion = true;
                    let prod = resQlin.productos[a];
                    let resprodi 	= [];
                    let resconsu 	= [];
                    
                    for(var e = 0; e< prod.length; e++){
                        let listprodi = {};
                        let listconsu = {};
                        if(prod[e].TS4_Tipo_QuoteLine__c == 'Producto'){
                            listprodi.TS4_Costo_maquina__c  = prod[e].TS4_Costo_maquina__c ;
                            listprodi.Costo__c  = prod[e].Costo__c  ;
                            listprodi.Id  = prod[e].Id ;
                            listprodi.Name   = prod[e].Name;
                            resprodi.push(listprodi);  
                        }
                        else
                        { 
                            listconsu.Cantidad__c   = prod[e].UnidadMedida__c;
                            listconsu.Cortesia__c   = prod[e].Cortesia__c  ;
                            listconsu.Costo__c  = prod[e].Costo__c  ;
                            listconsu.TS4_Producto__c  = prod[e].Id ;
                            listconsu.Name   = prod[e].Name;
                            resconsu.push(listconsu); 
                        }
                    }
                    mapValuesObj.push({"Id": resQlin.qli[a].Product2.Id, "name": resQlin.qli[a].Product2.Name, "IdPriceEntry": resQlin.qli[a].PricebookEntryId , "Unitprice": resQlin.qli[a].UnitPrice, "QuoteLineItemId": resQlin.qli[a].Id, "lstProduct": resprodi, "lstAccesorios": resconsu,  "cantidadaces": 1,"lookupMaquina": verMaquina, "lstVasos" : [], "lstconfig" : []});
                    
                }
                
                helper.cargaDatosMaquina(component, mapValuesObj);
                component.set('v.guardar',1);
            }
        });
        $A.enqueueAction(action);  
    },  
    
})