({
    searchHelper : function(component,event,getInputkeyWord) {
        
        var action = component.get("c.fetchLookUpValues");
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'queryString' : component.get("v.queryString"),
            'queryFilter' :  component.get("v.queryFilter"),
            'fieldsToSearch' : component.get("v.fieldsToSearch")
        });
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No se encontraron registros...');
                } else {
                    component.set("v.Message", '');
                }
                component.set("v.listOfSearchRecords", storeResponse);
            }
            
        }); 
        $A.enqueueAction(action);
        
    },
})