({
    retrieveData : function(component) {
        var action = component.get('c.searchAccount');
        action.setParams({
            recordId: component.get('v.recordId')
        });
        action.setCallback(this,function(a){
            var registro = a.getReturnValue();
            component.set('v.record',registro);
            if(registro != null){
                if(registro.TS4_CodigoPostal__r != undefined){
                    console.log(registro.TS4_CodigoPostal__r);
                    var cp = {};
                    cp.val = {};
                    cp.val.d_asenta__c = registro.TS4_CodigoPostal__r.d_asenta__c;
                    cp.val.Id = registro.TS4_CodigoPostal__c;
                    cp.val.D_mnpio__c = registro.TS4_CodigoPostal__r.D_mnpio__c;
                    cp.val.d_ciudad__c = registro.TS4_CodigoPostal__r.d_ciudad__c;
                    cp.val.d_estado__c = registro.TS4_CodigoPostal__r.d_estado__c;
                    cp.text = registro.TS4_CodigoPostal__r.d_codigo__c +', '+registro.TS4_CodigoPostal__r.d_asenta__c +', '+registro.TS4_CodigoPostal__r.D_mnpio__c +', '+registro.TS4_CodigoPostal__r.d_estado__c +', '+registro.TS4_CodigoPostal__r.d_ciudad__c;
                    cp.objName = 'TS4_CatalogoCP__c';
                    component.set('v.codigopostal',cp);
                }                
            }else{
                component.set('v.record',{});
            }
        });
        $A.enqueueAction(action); 
    },
    
    saveData : function(component){
        if(component.get('v.codigopostal') != null) {
            var registro = component.get('v.record');
            var Id_cuenta = component.get('v.recordId');
            var cp = component.get('v.codigopostal');
            registro.sobjecttype='TS4_Direcciones__c';
            registro.TS4_CodigoPostal__c = cp.val.Id;
            registro.Cuenta__c = Id_cuenta;
            registro.TS4_TipoDireccion__c  = 'Dirección Fiscal';
            
            console.log(registro);
            
            var action = component.get('c.saveAccount');
            action.setParams({
                direccion: registro
            });
            action.setCallback(this,function(a){
                if(a.getReturnValue() == 'OK'){
                    component.find('notifLib').showNotice({
                        "variant": "succes",
                        "header": "Guardado exitoso!",
                        "message": "El registro ha sido actualizado correctamente"
                    });
                }else{
                    component.find('notifLib').showNotice({
                        "variant": "error",
                        "header": "Tuvimos un problema",
                        "message": "Desafortunadamente no pudimos actualizar el registro, contacta al administrador"
                    });
                }
            });
            $A.enqueueAction(action); 
        }else{
            component.find('notifLib').showNotice({
                        "variant": "warning",
                        "header": "Hace falta capturar un CP",
                    });
        }
    }
    
})