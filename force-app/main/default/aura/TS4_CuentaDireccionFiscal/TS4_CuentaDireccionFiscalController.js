({
	doInit : function(component, event, helper) {
		helper.retrieveData(component);
	},
    save : function(component, event, helper){
        helper.saveData(component);
    }
})