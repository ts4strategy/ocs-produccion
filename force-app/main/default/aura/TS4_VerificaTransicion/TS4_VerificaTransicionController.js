({
    doInit : function(cmp, evn, hlp) {
        const getInitData = cmp.get("c.getInitData");
        getInitData.setParams({"opportunityId": cmp.get('v.recordId'), objName : cmp.get("v.nombreObjeto")});
        hlp.promesa(cmp, getInitData).then($A.getCallback(function(rltData){
        console.log(rltData)
            cmp.set('v.record', rltData);
            cmp.set('v.Contrato', rltData.contrato);
            cmp.set("v.lookupOwner", rltData.rltOpp.Owner);
            
            if(rltData.contrato.TS4_Referido__r != undefined){
                cmp.set("v.lookupReference", rltData.contrato.TS4_Referido__r);
            }
            
            if(rltData.rltCobranza != undefined){
                let ContactsPayment = [];
                rltData.rltCobranza.forEach(function(contact){
                    ContactsPayment.push(contact.TS4_Contacto__r);
                });
                cmp.set("v.lookupContactsCP", ContactsPayment);
            }
            console.log(rltData[0]);
            console.log(rltData[1]);
            if(rltData.rltInstalationDirContac != undefined ){ hlp.fillTableIContacts(cmp); }            
        }));
    },
    
    openSection : function(cmp, evn, hlp){
        var openSections = evn.getParam('openSections');
        if(openSections.includes('C') && !cmp.get("v.sectionC")){
            cmp.set("v.sectionC", true);
            cmp.find("LkOppOwner").editMode();
            if(cmp.get("v.lookupReference").Id != undefined){
                cmp.find("LookupReferido").editMode();
            }
            cmp.set("v.Contrato", cmp.get("v.record").contrato);
        }
        
        if(openSections.includes('G') && !cmp.get("v.sectionG")){
            cmp.set("v.sectionG", true);
            cmp.find("LookupCP").editMode();
        }
        if(openSections.includes('D') && !cmp.get("v.sectionD")){
            cmp.set("v.sectionD", true);
            cmp.set("v.Contrato", cmp.get("v.record").contrato);
        }
        
    },
    
    newaddPAC4 : function(cmp, evn, hlp){
        let table = cmp.get("v.newDataTable");
        let sltAddress  = cmp.get("v.lookupAddress");
        if(sltAddress.TS4_DireccionFormateada_Tt__c == undefined){
            cmp.find('notifLib').showToast({ "variant" : "warning", "title": "Selecionar", "message": "No haz seleccionado la dirección." });
        }else{
            let sltAssets  = cmp.get("v.lookupAssets");
            if(sltAssets.length == 0){
                cmp.find('notifLib').showToast({ "variant" : "warning", "title": "Selecionar", "message": "No haz seleccionado un servicio." });
            }else{
                let sltContacts = cmp.get("v.lookupContacts");
                if(sltContacts.length == 0){
                    cmp.find('notifLib').showToast({ "variant" : "warning", "title": "Selecionar", "message": "No haz seleccionado un contacto." });
                }else{
                    let sltRecords = hlp.tableIC(cmp, sltAssets, sltAddress, sltContacts, table);
                    if(table.length == 0){
                        table.push(sltRecords);
                    }else{
                        let containsAddress = table.find(function(tableAddress){ return tableAddress.Id == sltRecords.Id; });
                        if(containsAddress){
                            sltRecords.assets.forEach(function(sltAsset){
                                let containsAsset = containsAddress.assets.find(function(addressAsset){ return addressAsset.Id == sltAsset.Id; });
                                if(containsAsset){
                                    sltRecords.instalationContacts.forEach(function(IC){
                                        let containsIC = containsAddress.instalationContacts.find(function(addressAssetIC){ return addressAssetIC.Id == IC.Id; });
                                        if(containsIC){
                                        }else{
                                            containsAddress.instalationContacts.push(IC);
                                        }
                                    });
                                }else{
                                    containsAddress.assets.push(sltAsset);
                                    sltRecords.instalationContacts.forEach(function(IC){
                                        let containsIC = containsAddress.instalationContacts.find(function(addressAssetIC){ return addressAssetIC.Id == IC.Id; });
                                        if(containsIC){
                                        }else{
                                            containsAddress.instalationContacts.push(IC);
                                        }
                                    });
                                }
                                
                            });
                        }else{
                            let sltRecords = hlp.tableIC(cmp, sltAssets, sltAddress, sltContacts, table);
                            table.push(sltRecords);
                        }
                    }
                    cmp.set("v.newDataTable", table);
                    cmp.find("Lookup2").daniel();
                    cmp.find("Lookup1").clear();
                    cmp.find("Lookup2").clear();
                    cmp.find("Lookup3").clear();
                }
            }
        }
    },
    
    
    editRow : function(cmp, evn, hlp) {
        let address = evn.getSource().get("v.value");
        cmp.set("v.lookupAddress", address);
        cmp.find("Lookup1").editMode();
        cmp.set("v.lookupAssets", address.assets);
        cmp.find("Lookup2").editMode();
        cmp.set("v.lookupContacts", address.instalationContacts);
        cmp.find("Lookup3").editMode();
        let title = evn.getSource().get("v.title");
        let table = cmp.get("v.newDataTable");
        let indxdt = title.replace('Modificar', '');
        table.splice(indxdt, 1);
        cmp.set('v.newDataTable', table);   
    },
    
    removeRow : function(component, event, helper) {
        let tableHierarchy = event.getSource().get("v.value").split(';');
        var originalArray = component.get("v.newDataTable");
        let sltAsset, sltAddress, sltContact, arrayToRemove;
        
        sltAddress = tableHierarchy[0];
        sltAsset = tableHierarchy[1];
        sltContact = tableHierarchy[2];
        
        switch(tableHierarchy.length){
            case 1 : 
                helper.removeIndexByKey(originalArray, 'Id', sltAddress);
        }
        
        var dd = component.get("v.newDataTable");
        component.set("v.newDataTable", dd);        
    },
        
    saveProgress : function(cmp, evn, hlp){
        cmp.find("CuentaDireccionFiscal").save();
    	hlp.updContract(cmp);
        hlp.saveAddress(cmp);
        hlp.saveAssets(cmp);
        hlp.saveIC(cmp);
        hlp.savePContacts(cmp);
        hlp.updAccount(cmp);
        cmp.find('notifLib').showToast({ "variant" : "success", "title": "Guardado exitoso", "message": "Se han guardado tus cambios" });
        $A.get("e.force:closeQuickAction").fire();
    },
         
    handleComponentEvent : function(cmp, event, helper) {	 
        var flagModal = event.getParam("sObjectName");
        if(flagModal === 'TS4_Direcciones__c')
            cmp.set("v.addressModal", true);
        if(flagModal === 'PContact'){
            cmp.find('contactModalcmp').set('v.contactType', 'PContact');
            cmp.set("v.contactModal", true);
        }
        if(flagModal === 'IContact'){
            cmp.find('contactModalcmp').set('v.contactType', 'IContact');
            cmp.set("v.contactModal", true);
        }
    },
    
    handleShowModal: function(component, event) {
        var buttonName = event.getSource().getLocalId();
        var header;
        if(buttonName == 'enviar'){
            header = "¿Deseas enviar tu contrato?";
        }

        $A.createComponent("c:TS4_OverlayLibraryModal", {"buttonName" : buttonName},
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   var modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       header: header,
                                       body: modalBody, 
                                       showCloseButton: false,
                                       cssClass: "TS4_ModalConfirmacion",
                                       closeCallback: function(ovl) {
                                           $A.get("e.force:closeQuickAction").fire();
                                       }
                                   }).then(function(overlay){
                                   });
                               }
                           });
    },
    
    handleApplicationEvent : function(cmp, event, hlp) {
        let missingFields = hlp.validateFields(cmp);
        var message = event.getParam("message");
        var buttonName = event.getParam("buttonName");
        if(message == 'Ok' && buttonName == 'enviar'){
            if( missingFields.length > 0){
                cmp.find('notifLib').showToast({ "variant" : "warning", "title": "Hace falta llenar campos", "message": missingFields.join(', ') });
            }else{
                let contract = cmp.get("v.Contrato");
                if(cmp.get('v.record').rltOpp.StageName == 'Sembrado'){
                    contract.Status = 'Sembrado';
                }else{
                    contract.Status = 'En aprobación';
                }
                hlp.updContract(cmp);
                hlp.saveAddress(cmp);
                hlp.saveAssets(cmp);
                hlp.saveIC(cmp);
                hlp.updAccount(cmp);
                cmp.find('notifLib').showToast({ "variant" : "success", "title": "Envío exitoso", "message": "Se ha enviado tu Contrato" });
                $A.get("e.force:closeQuickAction").fire();
            }
        }
        else if(message == 'Cancel'){
            
        }
    },
    
    closeQA : function(cmp, evn, hlp){
        $A.get("e.force:closeQuickAction").fire();
    },
    
    newRecordLookup : function(cmp, evt, hlp){
        var nameModalObj = evt.getParam("nameObject");
        var newRecord = evt.getParam("newRecord");
        
        if(nameModalObj == 'TS4_Direcciones__c'){
            cmp.set("v.lookupAddress", newRecord);
            cmp.find("Lookup1").editMode();
        }else if(nameModalObj == 'PContact'){
            let tmpArray = cmp.get("v.lookupContactsCP");
            tmpArray.push(newRecord);
            cmp.set("v.lookupContactsCP", tmpArray);
            cmp.find("LookupCP").editMode();
        }else if(nameModalObj == 'IContact'){
            let tmpArray = cmp.get("v.lookupContacts");
            tmpArray.push(newRecord);
            cmp.set("v.lookupContacts", tmpArray);
            cmp.find("Lookup3").editMode();
        }
    },
    
    openModel: function(component, event, helper) {
        let modal = event.getSource().get("v.name");
        let indxdt = modal.replace('modal', '');
        let title = event.getSource().get("v.title");
		console.log(title.Id)
        var action = component.get("c.getInitDataService");
        action.setParams({ quoteliId: title.Id});  
        action.setCallback(this, function(result){
            console.log(result.getReturnValue)
            let resQlin= result.getReturnValue();
            let mapproduct = [];
            let mapconsumible = [];
            component.set("v.registroconsu", false); 
            component.set("v.registro", false); 
            for(var x = 0; x < resQlin.length; x++){
                if(resQlin[x].TS4_Tipo_QuoteLine__c =="Maquina"){
                    component.set("v.lstmaquina",resQlin[x]);
                }else{
                    if(resQlin[x].TS4_Tipo_QuoteLine__c =="Producto"){
                        component.set("v.registro", true); 
                        mapproduct.push(resQlin[x])
                    }else{
                        component.set("v.registroconsu", true);
                        mapconsumible.push(resQlin[x])
                    }
                }
                component.set("v.lstprodconfig",mapproduct);
                component.set("v.lstaccesorio",mapconsumible);
            }
        });
        $A.enqueueAction(action);
		component.set("v.modal", title.Product2.Name);
		component.set("v.isOpen", true);
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    },

    
})