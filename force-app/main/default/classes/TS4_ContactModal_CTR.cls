public with sharing class TS4_ContactModal_CTR { 
    @AuraEnabled
    public static Contact createContact(Contact con){
        insert con;
        return [SELECT Id,Name,Phone,MobilePhone,Email FROM Contact WHERE Id =: con.Id];
    }
}