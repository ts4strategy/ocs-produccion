@isTest
public class TS4_Cotizador_Test {
    static testMethod void main(){
        Account account = new Account();
        account.Name = 'Macondo';
        insert account;
        
        Contact contact1 = new Contact();
        contact1.AccountId = account.Id;
        contact1.FirstName = 'Ursula';
        contact1.LastName = 'Iguaran';
        contact1.Phone = '5559343489';
        contact1.Email = 'uiguaran@macondo.co';
        contact1.Puesto__c = 'Coordinador';
        contact1.Area__c = 'Compras';
        
        TS4_ContactModal_CTR.createContact(contact1);
        
        Contact contact2 = new Contact();
        contact2.AccountId = account.Id;
        contact2.FirstName = 'Aureliano';
        contact2.LastName = 'Buendia';
        contact2.Phone = '5559343498';
        contact2.Email = 'abuendia@macondo.co';
        contact2.Puesto__c = 'Coordinador';
        contact2.Area__c = 'Compras';
        
        TS4_ContactModal_CTR.createContact(contact2);
        
        List<Contact> listPContacts = new List<Contact>();
        listPContacts.add(contact1);
                
        Opportunity opportunity = new Opportunity();
        opportunity.Name = 'Da igual porque se sustituye';
        opportunity.AccountId = account.Id;
        opportunity.CloseDate = System.today().addMonths(1);
        opportunity.ContactoPrincipal__c = contact1.Id;
        opportunity.Type = 'Negocio existente';
        opportunity.StageName='Sembrado';
        
        insert opportunity;
        
        Catalogo__c catalogo1 = new Catalogo__c();
        catalogo1.Name = 'Agua';
        catalogo1.Id__c = 3;
        catalogo1.TipoCatalogo__c = 'Operación';
        
        insert catalogo1;
        
        Catalogo__c catalogo2 = new Catalogo__c();
        catalogo2.Name = 'Water Logic';
        catalogo2.Id__c = 4;
        catalogo2.TipoCatalogo__c = 'Marca';
        
        insert catalogo2;
        
        Product2 product = new Product2();
        product.Name = 'WL2FW Anual $1,000.00';
        product.IsActive = true;
        product.ProductCode = '387';
        product.Operacion__c = catalogo1.Id;
        product.ServicioTipo__c = catalogo2.Id;
        product.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Maquina').getRecordTypeId();
        product.IsActive = true;  
        product.Costo__c = 5;
        insert product;
        
        Product2 productP = new Product2();
        productP.Name = 'WL2FW Anual $1,000.00';
        productP.ProductCode = '387';
        productP.IsActive = true;
        productP.Operacion__c = catalogo1.Id;
        productP.ServicioTipo__c = catalogo2.Id;
        productP.IsActive = true;  
        productP.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Producto').getRecordTypeId();
        productP.Costo__c = 5;
        productP.TS4_Vasos__c = '8 oz.';
        insert productP;
        
        PricebookEntry pricebookEntry = new PricebookEntry(); 
        pricebookEntry.IsActive = true;
        pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        pricebookEntry.Product2Id = productp.Id;
        pricebookEntry.UnitPrice = 1000;
        
        insert pricebookEntry;
        
        Quote quotes = new Quote();
        quotes.Name = 'Da igual porque se sustituye';
        quotes.OpportunityId = opportunity.Id;
        quotes.Pricebook2Id = Test.getStandardPricebookId();
        quotes.Sucursal__c = catalogo2.Id;
        
        insert quotes;

        //Cotizador obtener accesorio
        TS4_Cotizador.getAccesorio( productp.Id);
            
        TS4_ModeloConfiguracion__c modconfig = new TS4_ModeloConfiguracion__c ();
        modconfig.TS4_Producto__c = product.Id;
        modconfig.TS4_Configuracion__c  = '636 1';
		insert modconfig;

        //Cotizador obtener lista de configuracion
        TS4_Cotizador.getConfigList( product.Id);

        TS4_ProductoConfiguracion__c lstproduct = new TS4_ProductoConfiguracion__c ();
        lstproduct.TS4_Producto__c = productP.Id;
        lstproduct.TS4_Configuracion__c  = '636 1';
		insert lstproduct;
        //Cotizador obtener lista de productos que tienen la misma configuracion
        string configlst = '636 1';
        TS4_Cotizador.getProduct(configlst);  

        Product2 productM = new Product2();
        productM.Name = 'WL2FW Anual $1,000.00';
        productM.ProductCode = '387';
        productM.Operacion__c = catalogo1.Id;
        productM.ServicioTipo__c = catalogo2.Id;
        productM.IsActive = true;  
        productM.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Maquina').getRecordTypeId();
        productM.Costo__c = 5;
        productM.TS4_Vasos__c = '8 oz.';
        insert productM;

        //Cotizador obtener lista de vasos que tiene una maquina
        TS4_Cotizador.getListVasos(productM.Id);

        //Cotizador obtener le pricebook de la oportunidad y quote
        TS4_Cotizador.getInitData(quotes.Id);

        Id idprice =quotes.Pricebook2Id;
        TS4_Cotizador.saveopp(idprice, opportunity.id, quotes.Id);
        
        Product2 productC = new Product2();
        productC.Name = 'WL2FW Anual $1,000.00';
        productC.ProductCode = '387';
        productC.Operacion__c = catalogo1.Id;
        productC.ServicioTipo__c = catalogo2.Id;
        productC.IsActive = true;  
        productC.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Maquina').getRecordTypeId();
        productC.Costo__c = 5;
        productC.TS4_Vasos__c = '8 oz.';
        insert productC;

        
        QuoteLineItem qlineitem2 = new QuoteLineitem ();
        qlineitem2.QuoteId  = quotes.Id;
        qlineitem2.Product2Id = product.Id;
        qlineitem2.PricebookEntryId = pricebookEntry.Id;
        qlineitem2.UnitPrice = pricebookEntry.UnitPrice;
        qlineitem2.Quantity = 1;
        qlineitem2.TS4_Maquina__c = productM.Id;
 
        List<TS4_Productos_Relacionados__c> prodrelaConsum = new List<TS4_Productos_Relacionados__c>();
        TS4_Productos_Relacionados__c lstproductSetC = new TS4_Productos_Relacionados__c ();
        lstproductSetC.TS4_Precio_Cliente__c = 0;
        lstproductSetC.TS4_Tipo_QuoteLine__c  = 'Consumible';
        lstproductSetC.TS4_Producto__c  = productC.Id;
        lstproductSetC.TS4_Cortesia__c  = false;
        prodrelaConsum.add(lstproductSetC);

        List<TS4_Productos_Relacionados__c> prodrelaProduc = new List<TS4_Productos_Relacionados__c>();
        TS4_Productos_Relacionados__c lstproductSetP = new TS4_Productos_Relacionados__c ();
        lstproductSetP.TS4_Precio_Cliente__c = 0;
        lstproductSetP.TS4_Tipo_QuoteLine__c  = 'Producto';
        lstproductSetP.TS4_Producto__c  = productC.Id;
        lstproductSetP.TS4_Precio_Maquina__c  = 13;
        prodrelaProduc.add(lstproductSetP);
        
        TS4_Productos_Relacionados__c lstproductSetM = new TS4_Productos_Relacionados__c ();
        lstproductSetM.TS4_Precio_Cliente__c = 0;
        lstproductSetM.TS4_Tipo_QuoteLine__c  = 'Maquina';
        lstproductSetM.TS4_Producto__c  = productM.Id;
        lstproductSetM.TS4_Cortesia__c  = false;
        
		TS4_Cotizador.TS4Cotizador  savecot = new TS4_Cotizador.TS4Cotizador();
        savecot.qli=qlineitem2;
        savecot.maquina = lstproductSetM;
        savecot.productos = prodrelaProduc;
        savecot.consumibles = prodrelaConsum;
        
        List<TS4_Cotizador.TS4Cotizador > savecots = new List<TS4_Cotizador.TS4Cotizador >();
        savecots.add(savecot);
        TS4_Cotizador.TS4Cotizador.saveDataQuote(savecots);
                
        TS4_Cotizador.MostrarTS4Cotizador  mostrarcot = new TS4_Cotizador.MostrarTS4Cotizador();
        TS4_Cotizador.MostrarTS4Cotizador.getInitDataService(quotes.Id);

    }
}