public without sharing class TS4_CuentaDireccionFiscal {
	
    @auraEnabled
    public static TS4_Direcciones__c searchAccount(Id recordId){
        return [Select Id, TS4_Calle__c, TS4_NumeroExterior__c, TS4_NumeroInterior__c, 
                TS4_EntreCalles__c, TS4_CodigoPostal__r.d_codigo__c, TS4_CodigoPostal__r.d_asenta__c,
                TS4_CodigoPostal__r.D_mnpio__c,TS4_CodigoPostal__r.d_estado__c,TS4_CodigoPostal__r.d_ciudad__c, 
                TS4_CodigoPostal__c FROM TS4_Direcciones__c WHere Cuenta__c =: recordId AND TS4_TipoDireccion__c = 'Dirección Fiscal'];
    }
    
    @auraEnabled
    public static String saveAccount(TS4_Direcciones__c direccion){
        upsert direccion;
        return 'OK';
    }
}