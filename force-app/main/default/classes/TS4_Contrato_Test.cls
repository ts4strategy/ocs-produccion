@isTest
public class TS4_Contrato_Test {
    static testMethod void main(){
        Account account = new Account();
        account.Name = 'Macondo';
        insert account;
        
        Contact contact1 = new Contact();
        contact1.AccountId = account.Id;
        contact1.FirstName = 'Ursula';
        contact1.LastName = 'Iguaran';
        contact1.Phone = '5559343489';
        contact1.Email = 'uiguaran@macondo.co';
        contact1.Puesto__c = 'Coordinador';
        contact1.Area__c = 'Compras';
        
        TS4_ContactModal_CTR.createContact(contact1);
        
        Contact contact2 = new Contact();
        contact2.AccountId = account.Id;
        contact2.FirstName = 'Aureliano';
        contact2.LastName = 'Buendia';
        contact2.Phone = '5559343498';
        contact2.Email = 'abuendia@macondo.co';
        contact2.Puesto__c = 'Coordinador';
        contact2.Area__c = 'Compras';
        
        TS4_ContactModal_CTR.createContact(contact2);
        
        List<Contact> listPContacts = new List<Contact>();
        listPContacts.add(contact1);
                
        Opportunity opportunity = new Opportunity();
        opportunity.Name = 'Da igual porque se sustituye';
        opportunity.AccountId = account.Id;
        opportunity.CloseDate = System.today().addMonths(1);
        opportunity.ContactoPrincipal__c = contact1.Id;
        opportunity.Type = 'Negocio existente';
        opportunity.StageName='Sembrado';
        
        insert opportunity;
        
        Catalogo__c catalogo1 = new Catalogo__c();
        catalogo1.Name = 'Agua';
        catalogo1.Id__c = 3;
        catalogo1.TipoCatalogo__c = 'Operación';
        
        insert catalogo1;
        
        Catalogo__c catalogo2 = new Catalogo__c();
        catalogo2.Name = 'Water Logic';
        catalogo2.Id__c = 4;
        catalogo2.TipoCatalogo__c = 'Marca';
        
        insert catalogo2;
        
        Product2 product = new Product2();
        product.Name = 'WL2FW Anual $1,000.00';
        product.ProductCode = '387';
        product.Operacion__c = catalogo1.Id;
        product.ServicioTipo__c = catalogo2.Id;
        product.IsActive = true;
        //product.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Servicio').getRecordTypeId();
        
        insert product;
        
        PricebookEntry pricebookEntry = new PricebookEntry(); 
        pricebookEntry.IsActive = true;
        pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        pricebookEntry.Product2Id = product.Id;
        pricebookEntry.UnitPrice = 1000;
        
        insert pricebookEntry;
        
        Quote quotes = new Quote();
        quotes.Name = 'Da igual porque se sustituye';
        quotes.OpportunityId = opportunity.Id;
        quotes.Pricebook2Id = Test.getStandardPricebookId();
        quotes.Sucursal__c = catalogo2.Id;
        
        insert quotes;

        
        TS4_Contrato.RelatedRecords relatedRecords = TS4_Contrato.getInitData(opportunity.Id, 'Opportunity');
        
        TS4_CatalogoCP__c postalCode = new TS4_CatalogoCP__c();
        postalCode.Name = '09240 - Progresista';
        postalCode.d_codigo__c = '09240';
        postalCode.d_asenta__c = 'Progresista';
        postalCode.D_mnpio__c = 'Iztapalapa';
        postalCode.d_estado__c = 'CDMX';
        postalCode.d_ciudad__c = 'CDMX';
        
        AddressModal_CTR.createPostalCode(postalCode);
        
        TS4_Direcciones__c  address1 = new TS4_Direcciones__c();
        address1.TS4_Calle__c = 'Emiliano Zapata';
        address1.TS4_NumeroExterior__c = '13';
        address1.TS4_CodigoPostal__c = postalCode.Id;
        address1.TS4_TipoDireccion__c = 'Dirección Fiscal';
        address1.Cuenta__c = account.Id;
        
        AddressModal_CTR.createAddress(address1);
        
        TS4_Direcciones__c  address2 = new TS4_Direcciones__c();
        address2.TS4_Calle__c = 'Francisco Villa';
        address2.TS4_NumeroExterior__c = '13';
        address2.TS4_CodigoPostal__c = postalCode.Id;
        address2.TS4_TipoDireccion__c = 'Dirección Instalación';
        address2.Cuenta__c = account.Id;
        address2.TS4_Contrato__c = relatedRecords.contrato.Id;
        
        AddressModal_CTR.createAddress(address2);
        AddressModal_CTR.getSobjects('SELECT Id,Name FROM TS4_Direcciones__c');
        
        List<TS4_Direcciones__c> addresses = new List<TS4_Direcciones__c>();
        addresses.add(address1);
        
        TS4_Contrato.saveAddress(addresses, relatedRecords.contrato); 
        TS4_Contrato.savePContacts(listPContacts, relatedRecords.contrato);
        
        TS4_ContactoRelacionado__c contactoRelacionado1 = new TS4_ContactoRelacionado__c();
        contactoRelacionado1.Name = 'Instalación - '+contact1.Name;
        contactoRelacionado1.TS4_Direccion__c = address2.Id;
        contactoRelacionado1.TS4_Contrato__c = relatedRecords.contrato.Id;
        contactoRelacionado1.TS4_Contacto__c = contact1.Name;
        contactoRelacionado1.TS4_RolContacto_Pt__c = 'Instalación';
                
        List<TS4_ContactoRelacionado__c> listContactosRelacionados = new List<TS4_ContactoRelacionado__c>();
        listContactosRelacionados.add(contactoRelacionado1);
        
        TS4_Contrato.saveIContacts(listContactosRelacionados, relatedRecords.contrato);
        TS4_Contrato.upAccount(account);
        
        QuoteLineItem qlineitem2 = new QuoteLineitem ();
        qlineitem2.QuoteId  = quotes.Id;
        qlineitem2.Product2Id = product.Id;
        qlineitem2.PricebookEntryId = pricebookEntry.Id;
        qlineitem2.UnitPrice = pricebookEntry.UnitPrice;
        qlineitem2.Quantity = 1;
        qlineitem2.TS4_Contract__c =  relatedRecords.contrato.Id;
        
        insert qlineitem2; 
        
        TS4_Productos_relacionados__c producrelacionado = new TS4_Productos_relacionados__c ();
        producrelacionado.TS4_Producto__c = product.Id;
        producrelacionado.TS4_Producto_de_cotizacion__c = qlineitem2.Id;
        producrelacionado.TS4_Tipo_QuoteLine__c= 'Maquina';
        
        insert producrelacionado;
        
        List<QuoteLineItem> idQuoteLI = new List<QuoteLineItem>();
		QuoteLineItem qlineitem1 = new QuoteLineitem ();
        qlineitem1.QuoteId  = quotes.Id;
        qlineitem1.Product2Id = product.Id;
        qlineitem1.PricebookEntryId = pricebookEntry.Id;
        qlineitem1.UnitPrice = pricebookEntry.UnitPrice;
        qlineitem1.Quantity = 1;
        qlineitem1.Id = qlineitem2.Id;
        idQuoteLI.add(qlineitem1);
        System.debug(idQuoteLI);
        String direcciones = address1.id;
        
        
        
        TS4_Contrato.saveAssets(relatedRecords.contrato, idQuoteLI, direcciones);
        TS4_Contrato.getInitDataService(qlineitem2.Id);

        
        //TS4_Contrato.getInitData(relatedRecords.contrato.Id, 'Contract');
        
        relatedRecords.contrato.Status = 'Rechazado';
        relatedRecords.contrato.TS4_MotivoRechazo_TAL__c = 'Rechazado desde clase prueba';
        TS4_Contrato.upContract(relatedRecords.contrato);
        TS4_Contrato.getInitData(opportunity.Id, 'Opportunity');
        
        Opportunity opportunitys = new Opportunity();
        opportunitys.Id=opportunity.Id;
        opportunitys.StageName='Identificación de necesidades';
        Update opportunitys;
        TS4_Contrato.RelatedRecords.getInitData(opportunity.Id, 'Opportunity');
        

        
    }
}