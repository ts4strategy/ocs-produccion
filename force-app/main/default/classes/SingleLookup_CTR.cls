public without sharing class SingleLookup_CTR {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String queryString, Boolean queryFilter, String fieldsToSearch) {
        system.debug('ObjectName-->' + queryString);
        String searchKey = '%' + searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
      
        String sQuery = '';
        
        String filterFields = '';
        for(String temp : fieldsToSearch.split(',')){
            filterFields += temp + ' LIKE ' + '\''+searchKey+'\'' +',';
        }
        String[] values = filterFields.split(',');
        String result = String.join( values, ' OR ' );
        System.debug(result);
        
        if(queryFilter)
            sQuery =  queryString + ' AND ('+ result +') ORDER BY createdDate DESC LIMIT 5';
        else
            sQuery =  queryString + ' WHERE ('+ result +') ORDER BY createdDate DESC LIMIT 5';
        //String sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey order by createdDate DESC limit 5';
        System.debug(sQuery);
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
}