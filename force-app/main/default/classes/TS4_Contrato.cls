public without sharing class TS4_Contrato{
    
    public class RelatedRecords {
        @AuraEnabled public Account cuenta{get;set;}
        @AuraEnabled public Contract contrato{get;set;}
        @AuraEnabled public Opportunity rltOpp{get;set;}
        @AuraEnabled public List<OpportunityLineItem> rltOLIs{get;set;}
        @AuraEnabled public List<Asset> rltAssets{get;set;}
        @AuraEnabled public List<TS4_Direcciones__c> rltInstalationDirContac{get;set;}
        @AuraEnabled public List<QuoteLineItem> rltInstalationqli{get;set;}
        @AuraEnabled public List<TS4_ContactoRelacionado__c> rltCobranza{get;set;}
    }
    
    @AuraEnabled
    public static RelatedRecords getInitData(Id opportunityId, String objName){
        
        Id recordId = opportunityId;
         
        if(objName == 'Contract'){ recordId = [SELECT Id FROM Opportunity WHERE ContractId = :opportunityId][0].Id; }
        
        Opportunity rltRecs = [SELECT Id,Name,ContractId,AccountId,Owner.Id,Owner.Name,StageName,TS4_Referido__c,SyncedQuoteId,
                                      Account.Id,Account.Name,Account.RazonSocial__c,Account.RFC__c,
                                      Contract.Id,Contract.Status,Contract.TS4_Tipo_Pt__c,Contract.StartDate,
                                      Contract.TS4_PeriodoFacturacion_Pt__c,Contract.TS4_DiasCredito_Pt__c,
                                      Contract.TS4_FormaPago_Pt__c,Contract.TS4_RequiereOrdenCompra_Cx__c,Contract.EndDate,
                                      Contract.ContractTerm,Contract.TS4_Referido__c,Contract.TS4_Referido__r.Name,
                                      Contract.TS4_Referido__r.TS4_NumeroEmpleado_Tt__c,Contract.Description,
                               		  Contract.TS4_FechasHorariosPago_TA__c,Contract.TS4_PortalFacturacion_URL__c,
                                      Contract.TS4_NotasFacturacion_TA__c,Contract.TS4_DuracionContrato_Pt__c
                               FROM   Opportunity WHERE Id =: recordId];
        
        RelatedRecords rtnData = new RelatedRecords();        
        rtnData.cuenta = rltRecs.Account;
        rtnData.rltOpp = rltRecs; 
        rtnData.contrato = rltRecs.Contract;
        rtnData.rltOLIs = rltRecs.OpportunityLineItems;
        
        if(rtnData.contrato == null && (rltRecs.StageName == 'Sembrado' || rltRecs.StageName == 'Cerrada ganada')){
            System.debug('Enviar Evending: No existe Contrato, se comienza a crear');
            
            contract contratoTemporal = new Contract(AccountId = rltRecs.AccountId, StartDate = System.today(), Status = 'Borrador', TS4_Referido__c = rltRecs.TS4_Referido__c);
            insert contratoTemporal;
           
            rtnData.contrato = [Select Id,Contract.Status,TS4_Tipo_Pt__c,StartDate,
                                      TS4_PeriodoFacturacion_Pt__c,TS4_DiasCredito_Pt__c,
                                      TS4_FormaPago_Pt__c,TS4_RequiereOrdenCompra_Cx__c,EndDate,
                                      ContractTerm,TS4_Referido__c,TS4_Referido__r.Name,
                                      TS4_Referido__r.TS4_NumeroEmpleado_Tt__c,Description,
                               		  TS4_FechasHorariosPago_TA__c,TS4_PortalFacturacion_URL__c,
                                      TS4_NotasFacturacion_TA__c,TS4_DuracionContrato_Pt__c
                                FROM Contract
                                where Id =: contratoTemporal.Id];
            System.debug('Enviar Evending: Se ha creado el contrato ' + rtnData.contrato.Id);
            rtnData.rltOpp.ContractId = rtnData.contrato.Id;
            update rtnData.rltOpp;
            System.debug('Enviar Evending: Se ha agregado el contrato en la la opp ' + rtnData.rltOpp.Id);
            
            rtnData.rltAssets = new List<Asset>();
            
            insert rtnData.rltAssets;
        }else if(rtnData.contrato.Status == 'Rechazado'){
            System.debug('Enviar Evending: El Contrato fue rechazado se crea uno nuevo');
            List<Asset> assetsToUpdate = new List<Asset>();
            for(Asset asset : [SELECT Id,Name,TS4_DescripcionProd_FTt__c,Status FROM Asset WHERE TS4_Contrato__c = :rtnData.contrato.Id]){    
                System.debug(asset.Id);
                asset.Status = 'Rechazado por contrato';
                assetsToUpdate.add(asset);
            }
            
            update assetsToUpdate;
            
            /*-----*/
            System.debug('Enviar Evending: No existe Contrato, se comienza a crear');
            
            rtnData.contrato = new Contract(AccountId = rltRecs.AccountId, StartDate = System.today(), Status = 'Borrador');
            insert rtnData.contrato;
            System.debug('Enviar Evending: Se ha creado el contrato ' + rtnData.contrato.Id);
            rtnData.rltOpp.ContractId = rtnData.contrato.Id;
            update rtnData.rltOpp;
            System.debug('Enviar Evending: Se ha agregado el contrato en la la opp ' + rtnData.rltOpp.Id);
            
            rtnData.rltAssets = new List<Asset>();
            insert rtnData.rltAssets;
            /*---*/
                        
        }else{
            rtnData.rltInstalationDirContac = [
                SELECT Id,Name,TS4_DireccionFormateada_Tt__c,
                (SELECT  TS4_Contacto__r.Id, TS4_Contacto__r.Name, TS4_Contacto__r.Email, 
                 TS4_Contacto__r.Phone, TS4_Contacto__r.MobilePhone FROM ContactosRelacionados__r 
                 WHERE TS4_RolContacto_Pt__c ='Instalación' AND TS4_Contrato__c = :rtnData.contrato.Id)         
                FROM TS4_Direcciones__c WHERE TS4_Contrato__c = :rtnData.contrato.Id];   
            rtnData.rltInstalationqli = [SELECT ID, Product2Id, Product2.Id, Product2.Name, Product2.ProductCode, Subtotal,TS4_Direcciones__c,TS4_Cargainicial__c FROM QuoteLineItem  WHERE TS4_Contract__c  = :rtnData.contrato.Id]; 
            
            rtnData.rltCobranza = [SELECT Id,TS4_Contacto__c,TS4_Contrato__c,TS4_Contacto__r.Id,TS4_Contacto__r.Name,TS4_Contacto__r.Email FROM TS4_ContactoRelacionado__c WHERE TS4_RolContacto_Pt__c = 'Cobranza' AND TS4_Contrato__c  = :rtnData.contrato.Id];
        }
        return rtnData;
    }    
    //save Payment Contacts
    @AuraEnabled 
    public static void savePContacts(List<Contact> rltContacts, Contract con){
        System.debug('Save Contacs Payments');
        List<TS4_ContactoRelacionado__c> insCRs = new List<TS4_ContactoRelacionado__c>(); 
        
        delete [SELECT Id,TS4_Contacto__c,TS4_Contrato__c FROM TS4_ContactoRelacionado__c WHERE TS4_RolContacto_Pt__c = 'Cobranza' AND TS4_Contrato__c = :con.Id];
                
        for(Contact contact : rltContacts){
            TS4_ContactoRelacionado__c temp = new TS4_ContactoRelacionado__c();
            temp.Name = 'Cobranza - ' + contact.Name;
            temp.TS4_Contacto__c = contact.Id;
            temp.TS4_Contrato__c = con.Id;
            temp.TS4_RolContacto_Pt__c = 'Cobranza';
            insCRs.add(temp);
        }
        insert insCRs;
    }
    //save Instalation Contacts
    @AuraEnabled
    public static void saveIContacts(List<TS4_ContactoRelacionado__c> rltContacts, Contract con){
        System.debug('Save Instalation Contacts');
        delete [SELECT Id,TS4_Contacto__c,TS4_Contrato__c,TS4_Direccion__c FROM TS4_ContactoRelacionado__c WHERE TS4_RolContacto_Pt__c = 'Instalación' AND TS4_Contrato__c = :con.Id];
        
        insert rltContacts;
    }
    
    @AuraEnabled
    public static void saveAddress(List<TS4_Direcciones__c> adds, Contract con){
        System.debug('Save Address APEX');        
        List<TS4_Direcciones__c> oldAdds = new List<TS4_Direcciones__c>([SELECT Id,Name,TS4_Contrato__c FROM TS4_Direcciones__c WHERE TS4_Contrato__c = :con.Id]); 
        for(TS4_Direcciones__c add : oldAdds){
            add.TS4_Contrato__c = null;
        }
        update oldAdds;
        update adds;
    }
    
    @AuraEnabled
    public static void saveAssets(Contract con, List<QuoteLineItem> idQuoteLI, string direcciones){
        system.debug(idQuoteLI);
        String[] strdirec = direcciones.split(',');
        System.debug('Save Assets APEX');
        List<Asset> assets = new List<Asset> ();
        List<QuoteLineItem> updactivoqli = new List<QuoteLineItem> ();
        List<QuoteLineItem> updactualesqli = new List<QuoteLineItem> ();
        List<TS4_Productos_Relacionados__c> TS4_PR = new List<TS4_Productos_Relacionados__c> ();        
        List<Asset> assetdlt = ([SELECT Id FROM Asset WHERE TS4_Contrato__c  =: con.Id]);
        List<TS4_Productos_Relacionados__c> prldlt = new List<TS4_Productos_Relacionados__c> ();
        if(assetdlt.size() >0){
                delete assetdlt;
        }
        
        List<QuoteLineItem> qlis = ([SELECT Id FROM QuoteLineItem WHERE TS4_Contract__c =: con.Id]); 
        for(integer o = 0; o < qlis.size(); o++){  
            QuoteLineItem qliacc = new QuoteLineItem ();
            qliacc.Id = qlis[o].Id;
            qliacc.TS4_Contract__c = null; 
            qliacc.TS4_Cargainicial__c  = ''; 
            qliacc.TS4_Direcciones__c = null; 
            updactualesqli.add(qliacc);
        }
        update updactualesqli;
        
        for(integer a = 0; a < idQuoteLI.size(); a++){
            List<QuoteLineItem> qlisales = ([SELECT Id, Product2Id,Product2.Name FROM QuoteLineItem WHERE Id =: idQuoteLI[a].Id]); 
            List<Contract> AcconID = ([SELECT AccountId  FROM Contract WHERE Id =: con.Id]); 
	        for(integer x = 0; x < qlisales.size(); x++){
                Asset newass = new Asset();
                newass.TS4_Direccion__c  = strdirec[a];  
                newass.Product2Id = qlisales[x].Product2Id;
                newass.Name  = 'Activo: ' + qlisales[x].Product2.Name;
                newass.TS4_Contrato__c = con.Id;
                newass.Quantity = 1;
                newass.Price = idQuoteLI[a].Subtotal;  
                newass.AccountId = AcconID[0].AccountId;  
                assets.add(newass);
            }
        }
        upsert assets;
        for(integer e = 0; e< assets.size();e++){  
            List<QuoteLineItem> qli = ([SELECT Id FROM QuoteLineItem WHERE Id =: idQuoteLI[e].Id]); 
            for(integer o = 0; o < qli.size(); o++){  
                QuoteLineItem priqli = new QuoteLineItem ();
                priqli.id = qli[o].Id; 
                priqli.TS4_Contract__c = con.Id;
                priqli.TS4_Cargainicial__c = idQuoteLI[e].TS4_Cargainicial__c;
                priqli.TS4_Direcciones__c = strdirec[e];
                updactivoqli.add(priqli);
            }
            List<TS4_Productos_Relacionados__c> Sltdlt = [SELECT Id, TS4_Activo__c from TS4_Productos_Relacionados__c Where  TS4_Producto_de_cotizacion__c =: idQuoteLI[e].Id];            	
            for (integer i = 0;  i < Sltdlt.size(); i++){
                TS4_Productos_Relacionados__c prinsert = new TS4_Productos_Relacionados__c ();
                prinsert.id = Sltdlt[i].Id; 
                prinsert.TS4_Activo__c = assets[e].Id; 
                prinsert.TS4_CargaIncial__c = idQuoteLI[e].TS4_Cargainicial__c;
                TS4_PR.add(prinsert);
            }
        }
        upsert TS4_PR;
        upsert updactivoqli;
    }
    
    //actualiza contrato
    @AuraEnabled
    public static void upContract(Contract contrato){
        System.debug('Save Contract APEX');
        contrato.ContractTerm = contrato.ContractTerm;
        update contrato;
    }
    
    //actualiza cuenta
    @AuraEnabled
    public static void upAccount(Account acc){
        System.debug('Save Account APEX');
        update acc;
    }
    
    @AuraEnabled
    public static List <TS4_Productos_Relacionados__c> getInitDataService(Id quoteliId) {
        List<TS4_Productos_Relacionados__c> rltPRP = [SELECT Id, Name, TS4_Producto__r.Name, TS4_Tipo_de_vaso__c, TS4_Sucursal__c, TS4_Subsidio__c,TS4_Consumidor__c,TS4_Toma_de_agua__c,TS4_Configuracion__c ,TS4_Cortesia__c, TS4_Precio_Maquina__c, TS4_Precio_Cliente__c, TS4_Producto__c, TS4_Tipo_QuoteLine__c, TS4_Cantidad__c from TS4_Productos_Relacionados__c Where  TS4_Producto_de_cotizacion__c =: quoteliId ];
        system.debug(rltPRP);
        return rltPRP;
    }
}