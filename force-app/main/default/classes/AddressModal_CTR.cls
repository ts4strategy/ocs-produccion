public with sharing class AddressModal_CTR { 
    @AuraEnabled
    public static TS4_Direcciones__c createAddress(TS4_Direcciones__c address){
        insert address;
        return [SELECT Id,Name,TS4_DireccionFormateada_Tt__c FROM TS4_Direcciones__c WHERE Id = :address.Id];
    }
    
    @AuraEnabled
    public static TS4_CatalogoCP__c createPostalCode(TS4_CatalogoCP__c cp){
        insert cp;
        return [SELECT Id,Name,d_codigo__c,d_asenta__c,D_mnpio__c,d_ciudad__c,d_estado__c FROM TS4_CatalogoCP__c WHERE Id =: cp.Id];
    }
    
    @AuraEnabled
    public static List<sObject> getSobjects(String query){ 
        return Database.query(query);
    }
    
}