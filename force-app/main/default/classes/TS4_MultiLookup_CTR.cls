public without sharing class TS4_MultiLookup_CTR {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String queryString, List<sObject> ExcludeitemsList, Boolean queryFilter) {
        String searchKey = '%' + searchKeyWord + '%';
        List < sObject > returnList = new List < sObject > ();
        
        List<string> lstExcludeitems = new List<string>();
        for(sObject item : ExcludeitemsList ){ 
            lstExcludeitems.add(item.id);
        }
        
        String sQuery = '';
        if(queryFilter)
             sQuery = queryString + ' AND Name LIKE :searchKey AND Id NOT IN : lstExcludeitems ORDER BY createdDate DESC';
        else
            // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5 and exclude already selected records
             sQuery = queryString + ' WHERE Name LIKE :searchKey AND Id NOT IN : lstExcludeitems ORDER BY createdDate DESC';
        //String sQuery =  'SELECT id, Name FROM ' +ObjectName + ' WHERE Name LIKE :searchKey AND Id NOT IN : lstExcludeitems ORDER BY createdDate DESC LIMIT 5';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    
    @AuraEnabled
    public static List < sObject > fetchPicklistValues(String queryString, List<sObject> ExcludeitemsList, Boolean queryFilter) {

        List < sObject > returnList = new List < sObject > ();
        
        List<string> lstExcludeitems = new List<string>();
        for(sObject item : ExcludeitemsList ){
            lstExcludeitems.add(item.id);
        }
        
        //Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5 and exclude already selected records
        String sQuery = '';
        if(queryFilter)
            sQuery = queryString + ' AND Id NOT IN : lstExcludeitems ORDER BY createdDate DESC';
        else
            sQuery = queryString + ' WHERE Id NOT IN : lstExcludeitems ORDER BY createdDate DESC';
        
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    
    @AuraEnabled
    public static List <TS4_Productos_Relacionados__c> getInitDataService(Id quoteliId) {
        List<TS4_Productos_Relacionados__c> rltPRP = [SELECT Id, TS4_Producto__r.Name, TS4_Tipo_de_vaso__c, TS4_Sucursal__c, TS4_Subsidio__c,TS4_Consumidor__c,TS4_Toma_de_agua__c,TS4_Configuracion__c ,TS4_Cortesia__c, TS4_Precio_Maquina__c, TS4_Precio_Cliente__c, TS4_Producto__c, TS4_Tipo_QuoteLine__c, TS4_Cantidad__c from TS4_Productos_Relacionados__c Where  TS4_Producto_de_cotizacion__c =: quoteliId ];
        return rltPRP;
    }

}