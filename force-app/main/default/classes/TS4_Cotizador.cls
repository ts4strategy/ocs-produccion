public class TS4_Cotizador  {
    //Obtener las porductos por configuraciones configuraciones
    @AuraEnabled /*Enabled for LEx*/
    Public static List<Product2> getProduct( String configlst){
        List<TS4_ProductoConfiguracion__c> lstProductos  = [SELECT TS4_Producto__c, TS4_Producto__r.Name, TS4_Producto__r.Costo__c  FROM TS4_ProductoConfiguracion__c WHERE TS4_Configuracion__c =: configlst AND TS4_Producto__r.IsActive  = true];
        List<Product2> productos = new List<Product2>();
        for (Integer a = 0; a < lstProductos.size(); a++ ) {
            Product2 productoConfiguracion = new Product2();
            productoConfiguracion.id = lstProductos[a].TS4_Producto__c;
            productoConfiguracion.name = lstProductos[a].TS4_Producto__r.Name;
            productoConfiguracion.Costo__c = lstProductos[a].TS4_Producto__r.Costo__c;
            productoConfiguracion.TS4_Costo_maquina__c = 0;
            productos.add(productoConfiguracion);
        }
        return productos;    
    }    

    //Obtener informacion sobre los accesorios
    @AuraEnabled 
    Public static Product2 getAccesorio( String configlst){
        List<PricebookEntry> lstProductos  = [SELECT Product2Id, Product2.Name, UnitPrice FROM PricebookEntry WHERE Product2Id =: configlst AND Product2.IsActive  = true ];
        Product2 productoAccesorio = new Product2();
        for (Integer a = 0; a < 1 ; a++ ) {    
            productoAccesorio.id = lstProductos[a].Product2Id;
            productoAccesorio.name = lstProductos[a].Product2.Name;
            productoAccesorio.Costo__c  = lstProductos[a].UnitPrice;
            productoAccesorio.Cortesia__c   = false ;
        }
        return productoAccesorio;
    }
    
    //Obtener las configuraciones de las maquinas
    @AuraEnabled 
    	public static TS4_ModeloConfiguracion__c[] getConfigList( ID idmachene) {
            List<TS4_ModeloConfiguracion__c> returnConfiguraciones =[SELECT TS4_Configuracion__c FROM TS4_ModeloConfiguracion__c WHERE TS4_Producto__c =: idmachene AND TS4_Producto__r.IsActive  = true ];
            System.debug(idmachene);
            System.debug(returnConfiguraciones);
        	return returnConfiguraciones;   
        }
    
    //Obtener el tipo de vaso para cada maquina
    @AuraEnabled 
    public static List<string> getListVasos( ID idmachene) {
        //SE creo TS4_Vasos__c en product
        List<Product2> lstVasos  = [SELECT TS4_Vasos__c FROM Product2 WHERE id =: idmachene  ]; 
        List<string> lstvaso = new List<string> ();
        List<string> tvasos = new List<string> ();
        List<string> tvaso = new List<string> ();
 		for (Integer a = 0; a < lstVasos.size(); a++ ) {
            tvaso = lstVasos[a].TS4_Vasos__c.split(';');
        }
        return tvaso;    
    }
    
    //Obtener el pricebook
    @AuraEnabled
    
    public static Quote getInitData(Id quoteId){
        return [SELECT Id,Opportunity.Pricebook2Id,Opportunity.Pricebook2.Name from Quote Where Id =:quoteId];
    } 

    @AuraEnabled
    public static MostrarTS4Cotizador getInitDataService(Id quoteId){
        List<QuoteLineItem> rltQLI = [SELECT Id, Product2.Name, UnitPrice, QuoteId, PricebookEntryId from QuoteLineItem Where  QuoteId=:quoteId];
        MostrarTS4Cotizador rtnData = new MostrarTS4Cotizador();
        rtnData.qli = new List<QuoteLineItem>();
        rtnData.maquina = new List<TS4_Productos_Relacionados__c>();
        rtnData.productos = new List<List<Product2>>();
        //Servicio
        for (Integer a = 0; a < rltQLI.size(); a++ ) {
            rtnData.qli.add(
                new QuoteLineItem(Id = rltQLI[a].Id, 
                                  Product2 = rltQLI[a].Product2,
                                  UnitPrice = rltQLI[a].UnitPrice,
                                  QuoteId = rltQLI[a].QuoteId,
                                  PricebookEntryId = rltQLI[a].PricebookEntryId)
            );
            //Maquina
            List<TS4_Productos_Relacionados__c> rltPRM = [SELECT Id, TS4_Producto__r.Name, TS4_Tipo_de_vaso__c, TS4_Sucursal__c, TS4_Subsidio__c,TS4_Consumidor__c,TS4_Toma_de_agua__c,TS4_Configuracion__c  from TS4_Productos_Relacionados__c Where  TS4_Producto_de_cotizacion__c =: rltQLI[a].Id AND  TS4_Tipo_QuoteLine__c ='Maquina'];
            for (Integer e = 0; e < rltPRM.size(); e++ ) {
                rtnData.maquina.add(
                    new TS4_Productos_Relacionados__c(TS4_Producto__r = rltPRM[e].TS4_Producto__r,
                                                      TS4_Tipo_de_vaso__c = rltPRM[e].TS4_Tipo_de_vaso__c,
                                                      TS4_Sucursal__c = rltPRM[e].TS4_Sucursal__c,
                                                      TS4_Subsidio__c = rltPRM[e].TS4_Subsidio__c,
                                                      TS4_Consumidor__c = rltPRM[e].TS4_Consumidor__c,
                                                      TS4_Toma_de_agua__c = rltPRM[e].TS4_Toma_de_agua__c,
                                                      TS4_Configuracion__c = rltPRM[e].TS4_Configuracion__c
                                                      )
                );           
            }
            //Producto
            List<TS4_Productos_Relacionados__c> rltPRP = [SELECT Id, TS4_Cortesia__c, TS4_Precio_Maquina__c, TS4_Precio_Cliente__c, TS4_Producto__c, TS4_Tipo_QuoteLine__c, TS4_Cantidad__c from TS4_Productos_Relacionados__c Where  TS4_Producto_de_cotizacion__c =: rltQLI[a].Id AND  TS4_Tipo_QuoteLine__c !='Maquina'];
            List<Product2> retuListPRP = new List<Product2>();
            for (Integer i = 0; i < rltPRP.size(); i++ ) {
	            Product2 ListPrP = new Product2();
	            Product2 rltproduct = [SELECT Id, Name from Product2 Where  ID =: rltPRP[i].TS4_Producto__c];
                ListPrP.Name = rltproduct.Name;
                ListPrP.TS4_Costo_maquina__c = rltPRP[i].TS4_Precio_Maquina__c; 
                ListPrP.Costo__c = rltPRP[i].TS4_Precio_Cliente__c;
                ListPrP.Id = rltPRP[i].TS4_Producto__c;
                ListPrP.UnidadMedida__c  = rltPRP[i].TS4_Cantidad__c;
                ListPrP.Cortesia__c = rltPRP[i].TS4_Cortesia__c;
                ListPrP.TS4_Tipo_QuoteLine__c = rltPRP[i].TS4_Tipo_QuoteLine__c;
                retuListPRP.add(ListPrP);
            }      
            rtnData.productos.add(retuListPRP);
        }
        return rtnData;        
    } 

 @AuraEnabled
    public static TS4_Utils.Message saveopp(Id pricebookid, Id oppid, Id quoteid){
        Quote quo = new Quote ();
        quo.id = quoteid;
        quo.Pricebook2Id = pricebookid;
        Opportunity opp = new Opportunity ();
        TS4_Utils.Message returnmessage = new TS4_Utils.Message(); 
  		opp.id = oppid;
        opp.Pricebook2Id = pricebookid;
        try {
            update quo;
            update opp;
            returnmessage.status = 'OK';
            returnmessage.mensaje = 'Se ha actualizado correctamente';           
        }
        catch(exception er){
            returnmessage.status = 'Error';
            returnmessage.mensaje = 'Se ha producido el siguiente error: ' + er.getMessage();           
        }
        return returnmessage;
    } 

    
    @AuraEnabled
    public static TS4_Utils.Message saveDataQuote (List<TS4Cotizador> dataCotizador){      
        TS4_Utils.Message returnmessage = new TS4_Utils.Message(); 
        List<QuoteLineItem> listQLI = new List<QuoteLineItem>();
        List<TS4_Productos_Relacionados__c> listProductR = new List<TS4_Productos_Relacionados__c>();
        try {
            for (TS4Cotizador ts4Cot : dataCotizador){
                List<QuoteLineItem> Sltqli = [SELECT Id from QuoteLineItem Where  QuoteId =: ts4Cot.qli.QuoteId];
                delete Sltqli;
                listQLI.add(ts4Cot.qli);
                List<TS4_Productos_Relacionados__c> Sltdlt = [SELECT Id from TS4_Productos_Relacionados__c Where  TS4_Producto_de_cotizacion__c =: ts4Cot.qli.id];
                delete Sltdlt;
            }
            upsert listQLI;
            for(integer e =0; e<dataCotizador.size();e++){             
                dataCotizador[e].maquina.TS4_Producto_de_cotizacion__c = listQLI[e].Id; 
                listProductR.add(dataCotizador[e].maquina);
                String idQLI = listQLI[e].Id;
                for(integer a =0; a<dataCotizador[e].productos.size();a++){
                    dataCotizador[e].productos[a].TS4_Producto_de_cotizacion__c = idQLI; 
                    listProductR.add(dataCotizador[e].productos[a]);
                }
                for(integer a =0; a<dataCotizador[e].consumibles.size();a++){
                    dataCotizador[e].consumibles[a].TS4_Producto_de_cotizacion__c = idQLI; 
                    listProductR.add(dataCotizador[e].consumibles[a]);
                }
            }
            upsert listProductR;
            returnmessage.status = 'OK';
            returnmessage.mensaje = 'Se ha actualizado correctamente';           
        }
        catch(exception er){
            returnmessage.status = 'Error';
            returnmessage.mensaje = 'Se ha producido el siguiente error: ' + er.getMessage();           
        }
        return returnmessage;
    }

    Public Class MostrarTS4Cotizador {
        @auraEnabled public List<QuoteLineItem> qli {get;set;}
        @auraEnabled public List<TS4_Productos_Relacionados__c> maquina {get;set;}
        @auraEnabled public List<List<Product2>> productos {get;set;}
    }
    
    
    Public Class TS4Cotizador {
        @auraEnabled public QuoteLineItem qli {get;set;}
        @auraEnabled public TS4_Productos_Relacionados__c maquina {get;set;}
        @auraEnabled public List<TS4_Productos_Relacionados__c> productos {get;set;}
        @auraEnabled public List<TS4_Productos_Relacionados__c> consumibles {get;set;}
    }
}